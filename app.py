def union(set1, set2):
    return set(set1) | set(set2)

def intersection(set1, set2):
    return set(set1) & set(set2)

def difference(set1, set2):
    return set(set1) - set(set2)

def symmetric_difference(set1, set2):
    return set(set1) ^ set(set2)

def cartesian_product(set1, set2):
    return [(x, y) for x in set1 for y in set2]

def print_set(set_result):
    print("{", end="")
    print(*set_result, sep=", ", end="")
    print("}")


set1 = input("Введіть першу множину (елементи через кому): ").split(",")
set2 = input("Введіть другу множину (елементи через кому): ").split(",")

union_result = union(set1, set2)
intersection_result = intersection(set1, set2)
difference_result = difference(set1, set2)
symmetric_difference_result = symmetric_difference(set1, set2)
cartesian_product_result = cartesian_product(set1, set2)

print("Об'єднання множин:")
print_set(union_result)

print("Перетин множин:")
print_set(intersection_result)

print("Різниця множин:")
print_set(difference_result)

print("Симетрична різниця множин:")
print_set(symmetric_difference_result)

print("Декартов добуток множин:")
print_set(cartesian_product_result)